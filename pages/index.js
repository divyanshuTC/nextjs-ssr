import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';


const styles = theme => ({
  bodyContainer: {
    padding: 8 * theme.spacing.unit
  },
  [theme.breakpoints.down('sm')]: {
    bodyContainer: {
      padding: 4 * theme.spacing.unit
    }
  }
});


class App extends React.Component {

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.bodyContainer}>

        <Typography
          variant="display1"
          align="center"
          gutterBottom
          style={{ 'fontWeight': '800' }}>
          Disclaimer
        </Typography>

        <br />

        <Typography
          variant="body1"
          gutterBottom
          style={{ textAlign: 'justify' }}>
          LoanCompareIndia intends to provide clear information about laon products and services. The information and data in the Website are generic in nature. Our efforts are to offer accurate and responsible data. We are not responsible for any sort of discrepancies.<br /><br />

          There is no purpose of violating any copyright or intellectual copyrights issues. All information provided on the portal LoanCompareIndia is subject to the discretion of the same and is likely to change without any notice. Though, any changes in public utility will be communicated immediately in our portal.<br /><br />

          We have tried to maintain high standards in quality, clarity, and accuracy of the material posted on the portal. LoanCompareIndia is not legally responsible for the same in any matter whatsoever. Employees, partners, and associated staff of LoanCompareIndia are not accountable for any loss, harm, or damage due to usage of information from the portal. Customers are advised to use their own discretion in such matters. The information provided on the portal is of financial, insurance, and legal nature. It is a mutual understanding that customers association with the portal will be at the customer's preference and risk.<br /><br />

          Visitors to this Website/portal and every third party is hereby informed that the owners of this Website/portal, are the intermediaries of the banks, financial institutions and mutual funds/AMC’s, whose products are dealt with in this Website/portal. It is made abundantly clear that LoanCompareIndia and UnoIdeo Technology Private Limited, its parent company, its directors, shareholders, officers and employees and LoanCompareIndia.com are in no way responsible or liable for any one for his/her investment or credit buy decision(s) (availing any kind of loans or taking credit card through us), and every prospect/loan seeker/ investor/policy-holder shall be solely responsible for the consequences of his/her decision(s).<br /><br />

          BEWARE OF FRAUDULENT PHONE CALLS AND E-MAILS. LoanCompareIndia clarifies to public that LoanCompareIndia or its officials will never ask customers/visitors to deposit any money/cash for any of our services. Public receiving such phone calls/emails are requested to intimate LoanCompareIndia along with details at LoanCompareIndia@gmail.com<br /><br />

          LoanCompareIndia makes no warranties or representations, express or implied, on products offered through the platform. The Company accepts no liability for any damages or losses, however caused, in connection with the use of, or on the reliance of its product or related services and/or investments made through the platform. Terms and conditions of the website are applicable including all products terms and conditions.
        </Typography>

        <br /><br />

        <Typography
          variant="headline"
          gutterBottom>
          Disclaimer for Risk Profiling
        </Typography>

        <br />

        <Typography
          variant="body1"
          gutterBottom
          style={{ textAlign: 'justify' }}>
          <b>In the event you do undertake risk profiling, you agree and declare that:</b><br />
          You have read, understood and personally accomplished this entire questionnaire and that the answers You have given are true, accurate and complete. You understand that the results indicated and investment products suggested by LoanCompareIndia are only representation of my risk profile. You are aware that this type of investment does not provide guarantee against losses and therefore You shall not hold liable LoanCompareIndia, its directors, officers, representatives and employees for any claim, suit, action, loss, damage or expense, which might such indemnified persons may incur as a result of my decision to invest in products. This is also to confirm that you shall notify LoanCompareIndia should there be any change in my risk profile or the information provided by me.
        </Typography>

        <br />

        <Typography
          variant="body1"
          gutterBottom
          style={{ textAlign: 'justify' }}>
          <b>In the event, you undertake risk profiling but you choose to invest as per your own choice and discretion; and not per the recommendations made by LoanCompareIndia:</b><br />
          You hereby declare that You have read, understood and personally accomplished this entire questionnaire and that the answers You have given are accurate and complete. You hereby waive recommendations of investment products made by LoanCompareIndia based on the results of your risk assessment and have decided to invest instead in another fund which has a risk level that is higher(or lower) than what is recommended. You fully understand that you may be taking more risks in exchange for possible higher returns. You expressly agree to assume such risks. You shall indemnify and hold harmless LoanCompareIndia, its directors, officers, representatives and employees for any claim, suit, action, loss, damage or expense which might such indemnified persons and/or you may incur as a result of your decision to invest in products with higher (or lower) risks.
        </Typography>

        <br />
        <Typography
          variant="body1"
          gutterBottom
          style={{ textAlign: 'justify' }}>
          <b>In the event, you voluntary skip undertaking your risk profiling:</b><br />
          You hereby declare that you acknowledge that according to prevalent laws in India, an loan adviser has to perform the risk profiling of its customers and recommend loan products accordingly. You hereby waive such requirement of risk profiling as you desire to invest as per your own free choice and discretion; and you are aware that your investment decision does not guarantee against losses and therefore you shall not hold liable LoanCompareIndia, its directors, officers, representatives and employees for any claim, suit, action, loss, damage, expense or any breach of law in this context, which might such indemnified persons may incur as a result of your decision to invest in products.
        </Typography>

        <br /><br />

      </div>
    )
  }

}


export default withStyles(styles)(App);